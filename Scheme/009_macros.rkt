#lang racket

(define-syntax print-elem         ;define print-elem syntax extension for the current program
  (syntax-rules ()                ;it follows the following rules [ (pattern to match)(what to do on match) ]
    [(_ a) (display a)]
    ))

(define-syntax print-list
  (syntax-rules ()
    [(_ a b) (begin (print-elem a) (display " ") (print-elem b) (newline) )]
    [(_ a b ...) (begin (print-elem a) (display " ") (print-list b ...))]
    ))

(print-list 2 2 3)

;-----------------------------------------------------------------------------------------------------------------

(define-syntax swap
  (syntax-rules ()
    [(swap a b) (let([tmp a]) (set! a b) (set! b tmp))]
    ))

(define-syntax rotate
  (syntax-rules ()
    [(rotate a b) (swap a b)]
    [(rotate a b c) (begin
                      (swap b c)
                      (swap a b))]
    ))

(define-syntax recursive-rotate
  (syntax-rules ()
    [(recursive-rotate a b) (swap a b)]
    [(recursive-rotate a b c ...) (begin (swap a b) (recursive-rotate b c ...))]      ; ... is a pattern matching for already matched templates. rotate a b c ... means that we search for a b c a b c a b c ..
    ))

(displayln "display rotate a b")
(let ([x 0] [y 12])
  (print-list x y)
  (rotate x y)
  (print-list x y)
  )

(displayln "display rotate a b c")
(let ([x 0] [y 12] [z 23])
  (print-list x y z)
  (rotate x y z)
  (print-list x y z)   
  )

(displayln "display recursive-rotate a b c")
(let ([x 0] [y 12] [z 23])
  (print-list x y z)
  (recursive-rotate x y z)
  (print-list x y z)     
  )

(displayln "display recursive-rotate a b c d")
(let ([x 0] [y 12] [z 23] [zz 89])
  (print-list x y z zz)
  (recursive-rotate x y z zz)
  (print-list x y z zz)     
  )