-- A complex number is basically two real numbers in orthogonal directions.
data Complex = Complex !Float !Float -- the trailing bang induce strictness (removes lazyness)

-- We need to convert the complex to a string to output it in REPL
instance Show Complex where
    show (Complex a b) = show ( (show a) ++ " + " ++ (show b) ++ "i")

instance Eq Complex where
    (Complex a b) == (Complex c d) = if a == c && b == d
                                     then True
                                     else False
--  /= it's implemented automatically after == is implemented

makec :: Float -> Float -> Complex
makec a b = (Complex a b)

complexSum :: Complex -> Complex -> Complex
complexSum (Complex a b) (Complex c d) = (Complex (a+c) (b+d))
-- REPL call -> complexSum (Complex 22 3) (Complex 2 22)

euclideanDistance :: Complex -> Float
euclideanDistance (Complex a b) = sqrt (a*a + b*b)

instance Ord Complex where
    (Complex a b) > (Complex c d) = if euclideanDistance (Complex a b) > euclideanDistance (Complex c d)
                                    then True
                                    else False
    (Complex a b) >= (Complex c d) = if euclideanDistance (Complex a b) >= euclideanDistance (Complex c d)
                                     then True
                                     else False
    (Complex a b) < (Complex c d) = if euclideanDistance (Complex a b) < euclideanDistance (Complex c d)
                                    then True
                                    else False
    (Complex a b) <= (Complex c d) = if euclideanDistance (Complex a b) <= euclideanDistance (Complex c d)
                                     then True
                                     else False

conjugate ::Complex -> Complex
-- To change the sign you use the 'negate' function
conjugate (Complex a b) = (Complex a (negate b) )

mapComplex :: (Complex -> a) -> [Complex] -> [a]
-- (..) indicates a function, [..] indicates a list in the type signature
mapComplex _ [] = []
mapComplex f (x:xs) = f x : mapComplex f xs
{-
	REPL call for lists:
		1)	[1, 2, 3, 4, 5]
		2)	1 : 2 : 3 : 4 : 5 : []
	Numbers can be replaced with every type that you need
-}

-- Having a map implemented, Complex is a Functor
-- WRONG! Having a map isn't enough to be a Functor, the reverse is true though.
-- Functors implements the fmap function, which have type:
-- fmap :: (a -> b) -> f a -> f b
-- and the functor is f. Here we don't have a container f (like Maybe could be).
-- It's obvious if we look at the type of Complex. It's not Complex a = ..., just Complex.
-- So Complex is not a container, thus cannot be a Functor.