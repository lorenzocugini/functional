-- : is the prepend operator, used to build lists.  x : y  ->  [x,y]

-- func takes a list and returns a list
func :: [a] -> [a]
-- Taking an empty list, return an empty list
func [] = []
-- Only one element in the list, return that element
func [x] = [x]
-- The list has at least two elements, return the first element
func (x:xs) = [x]


swap [] = []
swap [x] = [x]
-- swap(x:xs) if the first element is greater than the second (head xs), return those two swapped
swap (x:xs) | x > head xs = head xs:[x]
            | otherwise = [0,0]


doit [] = []
doit [x] = [x]
-- head xs returns the first element of xs
-- tail xs returns all but the first element of xs
doit (x:xs) | x > head xs = tail xs
            | x <= head xs = xs


inListSwap [] = []
inListSwap [x] = [x]
-- if [1,2] ok, if [2,1] swap them. The whole x : tail xs is to preserve the rest of the list
-- [1,2,3,4] ok, [2,1,4,3] -> [1,2,4,3]
inListSwap (x:xs) = if x > head xs
                    then head xs : (x : tail xs)
                    else x:xs