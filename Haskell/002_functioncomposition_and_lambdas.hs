{-
	Function composition acts like in maths -->  f.g(x) = f( g(x) )
	We apply the +1 before the *2
-}
dd = (*2) . (+1)

mathstuff :: Int -> Int -> Int
mathstuff = \a -> \b -> a + b