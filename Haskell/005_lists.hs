-- Courtesy of https://wiki.haskell.org/99_questions/1_to_10
-- #1
lastElement :: [a] -> a
lastElement [] = error "Empty list"
lastElement [x] = x
lastElement (_:xs) = lastElement(xs)

-- #2
lastButOneElement :: [a] -> a
lastButOneElement [] = error "Empty list"
lastButOneElement [x] = error "Singular list"
lastButOneElement [x,y] = x
lastButOneElement (_:xs) = lastButOneElement(xs)

-- #3
kthElement :: [a] -> Int -> a
kthElement [] _ = error "Empty list"
kthElement list n = list !! (n-1)       -- Haskell needs parenthesis on n-1 since with an operation you don't correctly match the pattern

-- #4
listLength :: [a] -> Int
listLength list = length list

-- #5
listReverse :: [a] -> [a]
listReverse list = reverse list

-- #6
palindrome :: (Eq a) => [a] -> [a] -> Bool
{-  I must restrict on the type of accepted a. I can't restrict on [a] since it will make no sense
	to check if two lists are the same without checking their content. Here we say that a must be an
	instance of Eq - Equality class - which must implement '==' and '!=' -}
palindrome lst1 lst2 = lst1 == reverse lst2