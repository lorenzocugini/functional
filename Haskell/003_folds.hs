import Data.List

nums = [1..10]

{- fold right associative -}
-- foldr f z [] = z
-- foldr f z (x:xs) = f x (foldr f z xs)

folded_r_sum = foldr (+) 0 nums
folded_r_diff = foldr (-) 0 nums

{-
	foldr (+) 0 [1 2 3 4 5 6 7 8 9 10]
	(+) 1 (foldr (+) 0 [2 3 4 5 6 7 8 9 10])		(+ 1 (foldr...))
	(+) 1 ( (+) 2 (foldr (+) [3 4 5 6 7 8 9 10]) )	(+ 1 (+ 2 (foldr...)))
	...
	(+ 1 (+ 2 (+ 3 (+ 4 (+ 5 (+ 6 (+ 7 (+ 8 (+ 9 (+ 10 0))))))))))

	If the operator is not right associative, this produces unwanted results.
	(-) is left associative.

	(- 1 (- 2 (- 3 (- 4 (- 5 (- 6 (- 7 (- 8 (- 9 (- 10 0))))))))))
	1 - (2 - (3 - (4 - (5 - (6 - (7 - (8 - (9 - (10 - 0)))))))))
	--> we start the proper evaluation in call-by-need style from the inner parenthesis, so foldr is efficient in Haskell since we'd normally have to start from there to
	    evaluate in mathematically.
	1 - (2 - (3 - (4 - (5 - (6 - (7 - (8 - (9 - (10)))))))))
	1 - (2 - (3 - (4 - (5 - (6 - (7 - (8 - (9 - 10))))))))
	1 - (2 - (3 - (4 - (5 - (6 - (7 - (8 - 9 + 10)))))))
	1 - (2 - (3 - (4 - (5 - (6 - (7 - 8 + 9 - 10))))))
	1 - (2 - (3 - (4 - (5 - (6 - 7 + 8 - 9 + 10)))))
	1 - (2 - (3 - (4 - (5 - 6 + 7 - 8 + 9 - 10))))
	1 - (2 - (3 - (4 - 5 + 6 - 7 + 8 - 9 + 10)))
	1 - (2 - (3 - 4 + 5 - 6 + 7 - 8 + 9 - 10))
	1 - (2 - 3 + 4 - 5 + 6 - 7 + 8 - 9 + 10)
	1 - 2 + 3 - 4 + 5 - 6 + 7 - 8 + 9 - 10		=>	-5
-}


{- fold left associative -}
-- foldl f z [] = z
-- foldl f z (x:xs) = foldl f (f z x) xs

folded_l_sum = foldl (+) 0 nums
folded_l_diff = foldl (-) 0 nums
{-
	foldl (+) 0 [1 2 3 4 5 6 7 8 9 10]
	foldl (+) ((+) 0 1) [2 3 4 5 6 7 8 9 10]
	--> this is NOT efficient in Haskell. We have already the (+ 0 1) ready to be evaluated, but since Haskell is lazy we'll have to wait until the foldl is finished to evaluate it.
	foldl (+) ((+) (+ 0 1) 2) [3 4 5 6 7 8 9 10]
	...
	(+ (+ (+ (+ (+ (+ (+ (+ (+ (+ 0 1) 2) 3) 4) 5) 6) 7) 8) 9) 10)

	(-) is right associative. So this works and yields -55, which is
	-1-2-3-4-5-6-7-8-9-10
-}




{- strict foldl -}
-- foldl' f z [] = z
-- foldl' f z (x:xs) = 	let z' = f z x
--						in seq z' (foldl' f z' xs)

folded_l_strict_sum = foldl' (+) 0 nums
folded_l_strict_diff = foldl' (-) 0 nums