{-
	Avoid tabs in Haskell, GHCI doesn't like it unless you use the -Wtabs.
-}

addNumber x y = x+y

isOdd n
 | mod n 2 == 0 = False -- Be careful to use the initial space, otherwise it wouldn't work
 | otherwise = True

data RPS = Rock | Paper | Scissors
jajanken Rock Rock = "Tie"
jajanken Paper Paper = "Tie"
jajanken Scissors Scissors = "Tie"
jajanken _ _ = "We only allow ties."