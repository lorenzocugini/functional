{- 	Lastname Firstname Age
	data SimplePerson = SimplePerson String String Int
 	guy = SimplePerson "Di Raziel" "Cadis Etrama" 900
 	This is OK, but it cannot be printed. Printing something requires the method SHOW.
	Show is a class from which we can derive!
-}

-- DATA CONSTRUCTORS

data SimplePerson = SimplePerson String String Int deriving (Show)
guy = SimplePerson "Di Raziel" "Cadis Etrama" 900

simplelastname :: SimplePerson -> String
simplelastname (SimplePerson ln _ _) = ln

simplefirstname :: SimplePerson -> String
simplefirstname (SimplePerson _ fn _) = fn

simpleage :: SimplePerson -> Int
simpleage (SimplePerson _ _ age) = age

-- There's a better way to express a structure

data Person = Person { lastname :: String
                     , firstname :: String
                     , age :: Int
                     , phoneNumber :: String
                     } deriving (Show)

guy2 = Person "Stein" "Franken" 820 "xxxxxxxxxx"



-- TYPE CONSTRUCTORS

-- a b c are type constructors!
data Car a b c = Car { company :: a,
                       model :: b,
                       year :: c
                     } deriving (Show)

-- Not worth it! Harder and not intuitive at all. We should parametrized the type only
-- when we need to be general enough. A general wrapper example is Maybe!
-- Maybe a : Nothing | Just a  is OK, since we may want to have Maybe Int, Maybe String, ...