module BizarreInfix where -- export everything

{- 	Definition of an operator goes through some keywords.
	First of all, operators have precedence. The precedence is a number from 0 to 9, the
	highest it is the more the operator has precedence with respect to its association (R/L/N).
	We also have to specify the direction of the association:

	Left : infixl	Right : infixr	None : infix
-}

infix 9 -*-
x -*- y = (x*x) + (y*y)