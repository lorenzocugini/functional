--Check if a list is in order (ascending order)
isOrdered :: (Ord a) => [a] -> Bool
isOrdered [] = True
isOrdered [x] = True
isOrdered (x:xs) | x > head xs = False
                 | x <= head xs = True && isOrdered xs

bubble :: (Ord a) => [a] -> [a]
bubble [] = []
bubble [x] = [x]
bubble (x:xs) = if x > head xs
                -- swap first and second and call bubble on the following part
                then head xs : bubble (x : tail xs)
                -- keep the first element still and call recursively
                else x : bubble xs

bubbleSort :: (Ord a) => [a] -> [a]
bubbleSort [] = []
bubbleSort [x] = [x]
bubbleSort xs = if isOrdered xs
                then xs
                else bubbleSort (bubble xs)