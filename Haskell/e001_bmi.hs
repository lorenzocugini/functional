bmi :: (RealFloat a) => a -> a -> String

bmi weight height
 | weight / (height ^ 2) < skinny = "Underweight!"
 | weight / (height ^ 2) <= normal = "Just normal!"
 | weight / (height ^ 2) <= fat = "Fatty!"
 | otherwise = "Well who knows?"
 where skinny = 18.5
       normal = 25.0
       fat = 30.0

-- Brief note: BMI needs the height in METRES, not CENTIMETERS!!