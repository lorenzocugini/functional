-- Example of lambdas
mySum :: Int -> Int -> Int
mySum a b = (\x y -> x+y) a b


-- Higher Order Functions

-- Example of implementation of a map function. NB: the map signature is always the same so it should
-- be remembered.
mapList :: (a -> b) -> [a] -> [b]
mapList _ [] = []
mapList f (x:xs) = f x : mapList f xs
-- The application of a map is available for Functor class.