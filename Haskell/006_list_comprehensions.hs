{- Try the same commands in REPL to see the output -}

-- list of x, x belongs to 1,2,3...,9,10
e010 = [x | x <- [1,2,3,4,5,6,7,8,9,10] ]
e011 = [x | x <- [1..10] ]

-- list of x, x belongs to 2,4,6...18.20
e020 = [x | x <- [2,4,6,8,10,12,14,16,18,20] ]
e021 = [x | x <- [2,4..20] ]
e022 = [2*x | x <-[1..10] ]

-- multiple conditions
e030 = [x | x <- [0..20] , 3*x < 50 ]
e031 = [x | x <- [0..100] , (mod x 7) == 0]
e032 = [x | x <- [10..20] , x /= 12, x /= 13] -- /= excludes a value. x /= 12 means exclude x = 12

-- double values
e040 = [ x*y | x <- [0..10] , y <- [0..10]]
e041 = [ (x,y) | x <- [1..5] , y <- [1..5] ]

-- string handling
st = "Hello, today we're going to hate Haskell."
e050 = [c | c <- st , elem c ['A'..'Z'] ]       -- only uppercases

-- pairs
adjectives = ["lazy", "grouchy", "happy", "scheming"]
nouns = ["frog", "man", "Jennifer"]
-- ++ concatenates list
e060 = [ adjective ++ " " ++ noun | adjective <- adjectives , noun <- nouns]

-- zip
-- a zip combines two lists by pairing elements in the same positions, discarding the remaining part of
-- the longer list. 1.. is an infinite list.
e070 = zip [1..] adjectives

-- pattern matching with @
firstLetter :: String -> String
firstLetter "" = "Empty string"
firstLetter all@(x:xs) = "First letter of " ++ all ++ " is " ++ [x]
-- xs@(y:ys) xs refers to the whole (unbroken) string, while y:ys is the usual split of first element - rest
-- why [x]? If we want to return a String, we need a [Char]. Breaking a string into x:xs means that x is a Char
-- and xs a [Char], that's why we need to concatenate [x]