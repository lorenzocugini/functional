{-
	Eq
	Class of equality (==) and unequality (/=). If we derive Eq, those two are implemented.
	
	(==) : 	sees if the data constructors match and if all the data contained inside matches by pairs.
			The drawback is that all contained data must derive Eq too.

	(/=) :	implemented as not (==)

	In the standard Prelude, Eq is defined as follows:

	class Eq a where
		(==) :: a -> a -> Bool
		(/=) :: a -> a -> Bool
		x == y = not (x /= y)
		x /= y = not (x == y) 

	Ord
	Subclass of Eq. It implements the family of ordering operators (>) (>=) (<) (<=). If no element is provided
	to explicit an ordering, the order of data constructors is used.
	data Bool = False | True deriving (Ord)
	--> True > False


	Show
	Printing class. Converts a value into a String.

-}

data TrafficLight = Red | Yellow | Green

-- instance keyword, meaning that TrafficLight it's a realization of the Eq class
instance Eq TrafficLight where
    Red == Red = True
    Yellow == Yellow = True
    Green == Green = True
    _ == _ = False


instance Show TrafficLight where
    show Red = "Red Light"
    show Yellow = "Yellow light"
    show Green = "Green light"