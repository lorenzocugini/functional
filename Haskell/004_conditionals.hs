describeLetter :: Char -> String

-- when in REPL, Char are between ' ', String between " "

describeLetter c 
 | c >= 'a' && c <= 'z' = if c == 'e' then 
    "The almighty letter e"
 else 
    "Lowercase letter"

 | c >= 'A' && c <= 'Z' =
   case c of
    'A' -> "The standard letter A"
    'E' -> "The almighty letter E"
    _  -> "Uppercase letter"

-- Careful with indentation. Haskell really is strict about this. If you indent
-- the don't care with a whitespace ' _' instead of '_', it will give a parse error on ->
 
 | otherwise = "Not an ASCII letter."