#lang racket
; Define in a purely functional way a procedure called revlt, which takes three lists, (x1 ... xL) (y1 ... yM) (z1 .. zN)
; and returns the list of vectors: (#(xO yO zO) ... #(x1 y1 z1)), where O ≥ 1 is the smallest among L, M, and N
;E.g. (revlt '(1 2 3) '(4 5 6 7) '(8 9 10))  is the list  '(#(3 6 10) #(2 5 9) #(1 4 8)).

;check how many times an element is inside a vector
(define (numIn vec x)
  (let ([count 0] [saved-cont #f] [iteration 0])
    (set! iteration (call/cc (lambda(cont) (set! saved-cont cont) (cont 0))))
    (when (eq? (vector-ref vec iteration) x)
        (set! count (+ count 1)))
    (if (eq? iteration (- (vector-length vec) 1))
        ;then
        count
        ;else
        (saved-cont (+ iteration 1)))))


(define (revlt lst1 lst2 lst3)
  ; Find O
  (let ([minlength (min (length lst1) (length lst2) (length lst3))])
    ; Create a support vector
    (let ([tmp_vec (make-vector minlength -1)] [saved-continuation #f])
      (call/cc
         (lambda(cont)
            (set! saved-continuation cont)
            (cont minlength)))

      (let ([i (- (vector-length tmp_vec) (numIn tmp_vec -1))]) ; position is 0, 1, 2, 3, ...
        (vector-set! tmp_vec i     
                   (quasiquote #(
                                 (unquote (list-ref lst1 i)) (unquote (list-ref lst2 i)) (unquote (list-ref lst3 i))
                                )))
        (when (< i (- minlength 1))
          (saved-continuation))
        )
       (reverse (vector->list tmp_vec))
     )))

;(display (revlt '(1 2 3) '(4 5 6 7) '(8 9 10)))


; The method above works, but it's too long.

(define (revltf l1 l2 l3)
  (let loop( [p1 l1] [p2 l2] [p3 l3] [out '()])   ;Prepare a label to construct the list
    (if (or (null? p1) (null? p2) (null? p3) )    ;If one list is null, we're over!
        ;then
        out
        ;else
        (let ( [x1 (car p1)] [x2 (car p2)] [x3 (car p3)] )
          (loop (cdr p1) (cdr p2) (cdr p3) (cons (vector x1 x2 x3) out))))))