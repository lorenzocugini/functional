{- A âdual listâ, or Dupl, is a pair of independent lists.
	1) Define a datatype for Dupl. Can it derive Show and/or Eq? If not, make Dupl an instance of both of them.
	2) Make Dupl an instance of Functor, Foldable, and Applicative.
-}

data Dupl a = Dupl [a] [a] deriving (Show, Eq)

duplfoldr :: (a -> b -> b) -> b -> (Dupl a) -> b
-- To make is an instance of Foldable, we just need to implement a fold. Make the foldr called upon l ++ r.
duplfoldr f z (Dupl l r) = foldr f z (l ++ r)

instance Foldable Dupl where
    foldr = duplfoldr

instance Functor Dupl where
    fmap f (Dupl l r) = Dupl (fmap f l) (fmap f r)

instance Applicative Dupl where
    pure x = Dupl [x] []
    (Dupl f1 f2) <*> (Dupl x1 x2) = Dupl (f1 <*> x1) (f2 <*> x2)